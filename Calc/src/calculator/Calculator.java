package calculator;

public class Calculator {

    public double getAddition(double firstArg, double secondArg) {
        return(firstArg + secondArg);
    }
    public double getSubtraction(double firstArg, double secondArg) {
        return(firstArg - secondArg);
    }
    public double getMultiplication(double firstArg, double secondArg) {
        return(firstArg * secondArg);
    }
    public double getDivision(double firstArg, double secondArg) {
        return(firstArg / secondArg);
    }


}
