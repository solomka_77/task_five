package calculator;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {

            Scanner in = new Scanner(System.in);
            System.out.print("Введите первое число: ");
            double firstArg = in.nextDouble();
            System.out.print("Введите второе число: ");
            double secondArg = in.nextDouble();
            System.out.println();

            Calculator calc = new Calculator();
            System.out.println("При сложениии получилось:  " + calc.getAddition(firstArg, secondArg));
            System.out.println("При вычитание получилось:  " + calc.getSubtraction(firstArg, secondArg));
            System.out.println("При умножении получилось:  " + calc.getMultiplication(firstArg, secondArg));
            try {
                if(secondArg == 0) {
                    throw new ArithmeticException();
                } else {
                    System.out.println("При делении получилось:  " + calc.getDivision(firstArg, secondArg));
                }
            } catch (ArithmeticException e) {
                System.out.println("На ноль делить не получается");
            }



        }
        catch (InputMismatchException e) {
            System.out.println("           CAUTION!!!       ");
            System.out.println("       БукаФки запрещены!!! ");
        }
    }
}

